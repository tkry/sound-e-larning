
//グローバル変数嫌われそう…
    var soundfilemax = 4//音ファイルが何個あるか
    var displayarray=[];//表示する順番を保存する配列
    var resultarray =[];//結果を保存しておく配列
    var soundnumber;//今何番を見ているか
    var endflag = false //最後まで終了したらtrueを入れる
    soundnumber = 0;


//表示する順番をセットするところ。ここをランダムにすれば流れる音の順番はランダムになる。
   for(var i=0;i<soundfilemax;i++){
       displayarray.push(i);
   } ;

//一番最初に実行されるもの
(function(){
    bootbox.confirm("こんにちは。右下のOKを押すと始まります。<br>zキーで音の再生、テンキーで選択が可能です。</br>",function(val){
        window.setTimeout(sound(),100);
    });   
}());


function sound(){
	// [ID:sound-file]の音声ファイルを再生[play()]する
    	if( typeof($('#soundfile'+ displayarray[soundnumber]).get(0).currentTime) != 'undefined' )
	{
		$('#soundfile'+ displayarray[soundnumber]).get(0).currentTime = 0;
	}
    $('#soundfile'+ displayarray[soundnumber]).get(0).play();
	$('#playbutton').fadeTo(1, 0.6);    
   // window.setTimeout(function(){$('#playbutton').fadeTo(1, 1.0);}, "500");
}

function movenext(){
    soundnumber ++;
    playingNumberRewrite();
    if(endflag == false){
        window.setTimeout(sound(),100);
    }else{
        bootbox.alert("終了になります。ご協力ありがとうございました。");
    }
}

function moveprev(){
    soundnumber --;
    playingNumberRewrite();
}

function chooseSelection(number){
    if(soundnumber>=soundfilemax-1){endflag = true;}
    
    if(endflag == false){
        resultarray[displayarray[soundnumber]]=number;
    }
    console.log(resultarray);
    movenext();
}

function playingNumberRewrite(){
    if(endflag == false){
    $("#playingnumber").text("Now Playing : " + (soundnumber+1));
    }else{
    $("#playingnumber").text("Finished!");
    }
}

//キーイベントの実装…同じことかくの？
$(window).keyup(function(e){
	if(e.keyCode == 90){sound();}//zで音がなる
   	if(e.keyCode == 49){chooseSelection(1);}//selected 1
   	if(e.keyCode == 50){chooseSelection(2);}//selected 2
    if(e.keyCode == 51){chooseSelection(3);}//selected 3
 	if(e.keyCode == 52){chooseSelection(4);}//selected 4
});

//マウスイベントの実装//画像に合わせたら光る
$(function(){
	$('a img').hover(
		function(){$(this).fadeTo(1, 0.8);},//0.6がalpha値
		function(){$(this).fadeTo(1, 1.0);}
	);
});
